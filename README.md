# Sito ILS

Questo è il repository del sito https://www.ils.org generato con [Jekyll](https://jekyllrb.com/).

## Requisiti per contribuire velocemente al codice

- avere Docker installato · esempio: `sudo apt install docker.io`
- avere un sistema operativo Unix-like qualsiasi e una tastiera · esempio: Debian

## Sviluppare in tempo reale

Per sviluppare in tempo reale, esegui una cosa del genere dalla cartella ils.org (è un singolo comando):

```
docker run \
	--rm \
	--tty=true \
	--interactive \
	--volume="$PWD":/srv/jekyll:Z \
	--volume="$PWD"/vendor/bundle:/usr/local/bundle:Z \
	--publish 4000:4000 \
	--env JEKYLL_ENV=production \
	--env JEKYLL_UID=1000 \
	--env JEKYLL_GID=1000 \
	jekyll/builder:3.8 jekyll serve
```

La prima volta che lo fai potresti dover aspettare alcuni minuti.

Il sito sarà disponibile in: `http://localhost:4000`

----

Se non hai alcuna intenzione di scrivere un comando Docker così lungo a mano ogni volta, puoi scaricare ILS Infrastructure:

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure

E puoi eseguire questi due comandi in successione, sempre dalla cartella ils.org:

```
source /opt/ils-infrastructure/projects/ils.org/env-production.sh
```

```
/opt/ils-infrastructure/cli/jekyll-docker.sh jekyll serve
```

(o qualcosa del genere a seconda di dove hai messo ILS Infrastructure)

## Creare una singola build statica

Normalmente questa cosa non interessa a nessuno, siccome puoi lavorare in tempo reale.

Per creare una singola build statica, eseguire questi due comandi in successione dalla cartella ils.org:

```
source /opt/ils-infrastructure/projects/ils.org/env-production.sh
```

```
/opt/ils-infrastructure/cli/jekyll-docker.sh jekyll build
```

Questo genera il sito statico nel percorso: `ils.org/_site` sul tuo computer.

## Aggiornare produzione a mano

Se per qualche motivo assurdo devi aggiornare produzione senza passare da GitLab CI/CD, puoi lanciare questi due comandi in successione dal tuo computer dalla cartella ils.org:

```
source /opt/ils-infrastructure/projects/ils.org/env-production.sh
```

```
/opt/ils-infrastructure/cli/deploy-remote.sh
```

Il tuo computer dovrà avere una chiave SSH abilitata in produzione.

## Continuous Deployment

Per aggiornare il sito https://ils.org normalmente non devi preoccuparti di nulla:

Quando i tuoi cambiamenti saranno uniti al ramo principale, il sito sarà aggiornato automaticamente.

Configurazione di Continuous Deployment in GitLab:

https://gitlab.com/ItalianLinuxSociety/ils.org/-/blob/master/.gitlab-ci.yml

In breve, questo è quello che fa GitLab quando riceve una nuova commit:

1. GitLab si collega via `ssh` a `servizi.linux.it` per usarlo come build server
2. da lì, effettuiamo la build del sito statico usando Jekyll dentro Docker
3. da lì, in caso di successo, aggiorniamo il server di produzione `ils.org` con `ssh` + `rsync`
4. GitLab fa sapere se è esploso tutto o meno

## Altri dettagli di Continuous Deployment

Domande frequenti:

* perché non facciamo fare tutta la catena di Deployment a GitLab.com?
    * apprezziamo GitLab ma non ci fidiamo totalmente di GitLab.com e preferiamo che gli artefatti di produzione siano prodotti lato nostro
    * vogliamo che le persone possano riprodurre l'esatto processo di build anche a mano 
    * vogliamo che i sistemisti possano riprodurre l'esatto processo di deploy anche a mano
    * vogliamo minimizzare le risorse hardware impegnate in GitLab.com perché vogliamo essere loro ospiti graditi
    * quando cambieranno i tariffari di esecuzione in GitLab.com noi saremo già al minimo uso e quindi minimi costi
* perché coinvolgere il server `servizi.linux.it`?
    * perché è un computer con SSH e Docker e quindi andava bene per fare una build
    * sicuramente non era possibile farlo sul server `worf` di produzione perché mettendoci Docker sarebbe esploso il volontario `md` che se ne occupa
* perché usate una immagine Docker _solo_ per eseguire `ssh`?
    * GitLab ha bisogno di una immagine docker per poter lanciare un comando come quello
* perché usate una immagine Docker basata su _Debian_, solo per eseguire `ssh`?
    * ci fidiamo _molto_ dei pacchetti Debian in produzione e quindi aveva senso essere coerenti anche qui
* sì ma perché usate una _vostra_ immagine Docker solo per avere Debian + ssh?
    * non ci risulta una immagine ufficiale di Debian con il pacchetto `ssh` pre-installato
    * inoltre non aveva molto senso lanciare, ogni volta, a runtime, un privision di `apt install ssh`. Farlo così spesso a caso è uno spreco di CPU, RAM, spreco dei mirror di Debian, spreco della banda locale, e quindi uno spreco di risorse ambientali facilmente evitabile.
    * inoltre, creare una immagine Debian con ciò che ci serve dentro (ssh-client) è stato semplice, ed è semplice tornare indietro, e le performance sono al massimo.

Grazie per ogni contributo al processo di deploy!

## Risorse

Le variabili d'ambiente per produzione:

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/projects/ils.org/env-production.sh

Lo script che esegue Jekyll e legge quelle variabili:

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/cli/jekyll-docker.sh

Lo script che fa build+deploy in produzione e legge quelle variabili:

https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/cli/deploy-remote.sh

Immagine Docker usata per lanciare Jekyll:

https://hub.docker.com/r/jekyll/builder/

Immagine Docker che GitLab usa per lanciare comandi via SSH:

https://hub.docker.com/r/italianlinuxsociety/debian_openssh_client

Grazie per ogni domanda su queste risorse!

## Segnalazioni

Pagina per aprire segnalazioni e richieste pubbliche (da preferire):

https://gitlab.com/ItalianLinuxSociety/ils.org/-/issues/new

Altri contatti:

https://www.ils.org/contatti/

## Licenza

Copyright (2020-2023) contributori di Italian Linux Society e ILS.org

Salvo ove diversamente indicato tutti i contenuti sono rilasciati in pubblico dominio, Creative Commons Zero.

https://creativecommons.org/publicdomain/zero/1.0/

Eccezioni: loghi di associazioni, di partner e sponsor. Contattarli per conoscere le relative licenze.

Il codice sorgente invece è rilasciato sotto licenza GNU Affero General Public License. In breve puoi fare qualsiasi cosa,
anche per scopi commerciali, a patto che rilasci le tue modifiche come software libero. Dettagli:

https://www.gnu.org/licenses/agpl-3.0.html
