---
layout: post
title: Linux Day 2016
created: 1476718272
---
<p style="text-align: center">
<img src="/assets/images/linuxday_logo.png" alt="Linux Day">
</p>

<p>
Sabato 22 ottobre si svolgerà in tutta Italia il <a href="https://www.linuxday.it/" rel="nofollow">Linux Day 2016</a>.
</p>

<p>
Giunta alla sedicesima edizione, la più popolare manifestazione nazionale per la promozione a Linux, al software libero ed alle libertà digitali è articolata in decine di eventi locali organizzati da <a href="https://lugmap.linux.it/">Linux Users Groups</a>, gruppi di volontari, associazioni, scuole ed aziende che, nello stesso giorno, allestiscono talk divulgativi, incontri di supporto tecnico ed altre attività volte a far conoscere e ad approfondire i temi chiave della consapevolezza digitale.
</p>

<p>
E, in particolare quest'anno, per farli conoscere ai più giovani, coloro che per mezzo di computer e smartphone quotidianamente vivono a contatto con la tecnologia ma che troppo spesso non ne comprendono le opportunità, i rischi e le implicazioni. Numerosi gruppi organizzatori hanno annesso alla propria manifestazione sessioni di "CoderDojo", workshop di programmazione elementare destinati a bambini e ragazzi dai 7 anni, per aiutarli a costruire il loro primo "software" in maniera facile e divertente.
</p>

<p>
Docenti ed insegnanti sono a loro volta invitati a partecipare, non solo alla giornata del Linux Day ma, più in generale, all'adozione di pratiche didattiche non passivamente basate su strumenti e nozioni ma sulla valorizzazione e l'elaborazione delle conoscenze: a questo proposito, sul sito <a href="https://scuola.linux.it/">scuola.linux.it</a> (<a href="{% link _posts/2016-09-06-scuola-linux-it.md %}">recentemente rinnovato</a>) si possono trovare numerosi riferimenti e links.
</p>

<p>
Italian Linux Society, che dal 2001 coordina il Linux Day nazionale, ringrazia tutti coloro che anche quest'anno hanno partecipato e parteciperanno attivamente alla manifestazione per la diffusione del software libero, i gruppi, i volontari, i relatori, nonché chi lo sostiene economicamente: <a rel="nofollow" href="http://www.lpi-italia.org/">Linux Professional Institute Italia</a>, <a rel="nofollow" href="http://www.ccinetwork.it/linux">Carrara Computing International</a>, <a rel="nofollow" href="https://yoctoproject.org/">Yocto Project</a> e <a rel="nofollow" href="http://koansoftware.com/">Koan</a>.
</p>
