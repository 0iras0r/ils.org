---
layout: post
title: Innovazione Libera!
created: 1373927105
---
In vista del <a href="https://www.linuxday.it/">Linux Day 2013</a>, quest'anno dedicato all'Innovazione ed al ruolo che il software libero e opensource ha nel contesto tecnologico, economico e sociale, <a href="/">Italian Linux Society</a> sta distribuendo un breve questionario - intitolato appunto "Innovazione Libera!" - rivolto alle numerose e varie startup del nostro Paese. L'indagine ha l'obiettivo di misurare e quantificare l'impatto che il modello di sviluppo aperto e condiviso ha nella ideazione, produzione e fruizione dell'innovazione - non solo digitale - in Italia, e costruire un quadro un poco più preciso su chi, come, quanto e perché adopera soluzione libere nello svolgimento delle proprie attività.

I risultati del questionario verranno poi aggregati in un rapporto finale pubblicato a settembre sul <a href="https://www.linuxday.it/">sito del Linux Day</a>, nel quale saranno anche menzionate le aziende che hanno partecipato. Non solo per rendere merito a chi contribuisce all'iniziativa, ma anche per garantire la massima trasparenza dell'operazione.

E trasparenza e credibilità sono i due fattori con i quali il questionario è stato assemblato e consegnato ai partecipanti: l'intestazione introduttiva raccomanda esplicitamente di essere sinceri nelle risposte, ovvero di non dichiarare cose non aderenti al vero al solo scopo di lusingarci, e nel raccogliere i nomi da coinvolgere  nell'indagine (a oggi circa 200) non è stata operata alcuna discriminazione nei confronti di aziende che già nel proprio sito menzionano l'utilizzo di tecnologie non aperte oppure espongono il logo di partnership con realtà notoriamente incentrate su software chiuso e proprietario. L'intento dell'indagine non è di sterile propaganda fine a sé stessa, come purtroppo capita talvolta per altre iniziative promosse nello stesso settore, bensì di analisi e valutazione - anche critica - del mondo open.

Come detto l'accesso al questionario è regolato da inviti nominativi spediti ai candidati individuati, in modo da evitare che sia compilato più volte o da soggetti non iscrivibili nel nostro campione di interesse statistico. Ma è ovviamente aperto a tutti coloro che pur lavorando in una giovane azienda non hanno ancora ricevuto il nostro appello, è sufficiente contattare l'indirizzo <a href="mailto:info@linuxday.it">info@linuxday.it</a> per essere accolti a braccia aperte!

Grazie a tutti coloro che vorranno darci una mano per arricchire la nostra base di dati e migliorare la nostra analisi, e più in generale grazie a chi ogni giorno dedica tempo e risorse per lo sviluppo di un Paese migliore.
