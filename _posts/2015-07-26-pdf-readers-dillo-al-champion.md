---
layout: post
title: 'PDF Readers: Dillo al Champion!'
created: 1437869021
---
Ancora troppo spesso sui siti delle pubbliche amministrazioni i documenti PDF pubblicati sono accompagnati da un esplicito invito all'utilizzo di una ben precisa applicazione per la loro consultazione, proprietaria e non sempre così aderente allo stesso standard PDF, facendo implicita pubblicità ad una singola azienda ed ignorando il resto del mercato (e soprattutto le alternative libere). Basta una rapida ricerca applicata sul sito di un qualsiasi comune (<a rel="nofollow" href="https://duckduckgo.com/?q=site%3Acomune.roma.it+acrobat+reader">qui un esempio per quello di Roma</a>) per individuare centinaia di riferimenti, o peggio ancora files nominalmente in formato PDF ma formattati con uno schema fuori standard e pertanto non interoperabile.

Risulta dunque necessario rinnovare e diramare l'invito originariamente promosso dalla campagna <a rel="nofollow" href="https://fsfe.org/campaigns/pdfreaders/pdfreaders.it.html">PDFReaders.org</a>, in corso già da qualche tempo in tutta Europa per opera di <a rel="nofollow" href="https://fsfe.org/">Free Software Foundation Europe</a>, atto a sollecitare le amministrazioni a rimuovere tali inopportuni e sconvenienti inviti, dannosi per la libera scelta e la libera concorrenza, ed adottare formati realmente standard e fruibili da tutti i cittadini.

Per far questo vogliamo, col vostro aiuto, chiedere nuovamente il coinvolgimento dei <a rel="nofollow" href="http://digitalchampions.it/">Digital Champions</a> - gli "ambasciatori digitali" incaricati di consigliare, guidare e supportare gli enti pubblici nel loro percorso di digitalizzazione - sfruttando la piattaforma "Dillo al Champion!", con la quale si può facilmente individuare il rappresentante più vicino e contattarlo mandandogli un messaggio su Twitter.

Segnalate, sollecitate e divulgate: per leggere i documenti PDF non esiste una sola applicazione!
