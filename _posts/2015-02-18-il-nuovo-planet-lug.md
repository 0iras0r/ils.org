---
layout: post
title: Il Nuovo Planet LUG
created: 1424263215
---
Da tempo immemore Italian Linux Society ospita un "planet", un aggregatore delle news provenienti dai siti dei Linux Users Groups italiani. Oggi ne è stato pubblicato un restyle, rinnovato nella grafica - resa coerente con gli altri siti del network ILS - ma soprattutto nel meccanismo di aggiornamento, che lo rende connesso ai contenuti della <a href="https://lugmap.linux.it/">LugMap</a> e dunque sempre autonomamente allineato per riflettere lo stato attuale della rete dei gruppi di supporto linuxaro.

Tra le altre cose, da questo costante flusso di news vengono estrapolate le segnalazioni degli eventi che finiscono poi nel <a href="https://lugmap.linux.it/eventi/" rel="nofollow">Calendario Eventi</a> (e conseguentemente notificati agli <a href="/newsletter">iscritti alla newsletter</a>).

Per fruire del nuovo Planet LUG è possibile consultare <a href="https://planet.linux.it/lug/" rel="nofollow">la homepage</a>, o sottoscrivere il <a href="https://planet.linux.it/lug/atom.xml" rel="nofollow">feed Atom</a> con il proprio <a rel="nofollow" href="https://en.wikipedia.org/wiki/Comparison_of_feed_aggregators">reader preferito</a>. Per gli entusiasti di Twitter, inoltre, ricordiamo <a rel="nofollow" href="https://twitter.com/LugMap/lists/news-dai-lug">la lista "News dai LUG"</a>, che raccoglie tutti gli account di microblogging dei LUG italici.

Un modo alternativo per essere sempre informati di quel che accade nella nostra comunità!
