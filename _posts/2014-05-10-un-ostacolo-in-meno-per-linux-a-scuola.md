---
layout: post
title: Un Ostacolo in Meno per Linux a Scuola
created: 1399746217
---
Un piccolo, grande aggiornamento per quanto riguarda l'uso di software libero a scuola.

Da alcuni anni agli studenti delle scuole italiane vengono somministrati questionari volti a valutare la qualita' e l'efficacia della didattica negli istituti, su iniziativa del Ministero per l'Istruzione e su supervisione dell'Istituto Nazionale per la Valutazione del Sistema Educativo (<a rel="nofollow" href="http://www.invalsi.it/">INVALSI</a>). Purtroppo pero' l'unico modo con cui ogni scuola poteva trasmettere all'ente gli esiti delle prove e' sinora stato un documento fruibile esclusivamente con il programma Microsoft Excel, pesantemente manipolato con funzionalita' non interoperabili con applicativi analoghi quali LibreOffice o OpenOffice, e dunque impossibile da utilizzare su piattaforme operative diverse da Microsoft Windows.
In passato cio' ha provocato gravi situazioni di imbarazzo e difficolta' a coloro che, con impegno e dedizione, spesso usando il proprio tempo libero e la propria pazienza, sono riusciti ad introdurre alternative libere (nonche' gratuite e legalmente ridistribuibili agli studenti a costo zero) nei rispettivi istituti scolastici, i quali si sono trovati impossibilitati a svolgere una operazione richiesta dall'organismo centrale.

Ma oggi, grazie a numerose richieste ed a numerosi solleciti - generati soprattutto dal sempre attivissimo <a rel="nofollow" href="http://wiildos.it/">gruppo del progetto WiiLD</a>, primo promotore del software libero nella scuola nostrana - INVALSI ha iniziato a distribuire una applicazione ad-hoc, cross-platform, che permette a tutti di dialogare digitalmente con l'ente per la raccolta delle informazioni. Un dettaglio di grande valore, destinato a rendere piu' facile l'adozione di soluzioni informatiche alternative e svincolate dal circolo vizioso del monopolio "de facto" che vige in tanti ambiti, non ultimo quello della scuola, pesando sui bilanci e sull'autonomia che dovrebbe essere garantita al delicato settore dell'istruzione.
Tutti coloro che operano o forniscono assistenza tecnica all'interno di una scuola sono invitati a prendere parte alla sperimentazione del nuovo programma, inviando una mail di richiesta informazioni a prove2014@invalsi.it.

Un pezzetto alla volta, auspichiamo in una scuola sempre piu' aperta, libera, partecipata e consapevole, anche in campo tecnologico.
