---
layout: post
title: 2015 con ILS
created: 1450630588
---
Volge al termine un ennesimo anno di attività per Italian Linux Society, come sempre all'insegna del sostegno al software libero e delle tante realtà locali che contribuiscono attivamente al medesimo scopo.

Un breve riassunto di quanto compiuto negli ultimi 12 mesi:

<ul>
  <li>quasi 10000 euro raccolti grazie all'iniziativa <a href="http://donazioni.linux.it/">donazioni.linux.it</a> e destinati allo sviluppo di software libero per la scuola, di cui 6000 messi a disposizione direttamente dai soci di Italian Linux Society. Lo sviluppo di Lightboard, il primo dei progetti finanziati, procede sul <a rel="nofollow" href="https://github.com/giovanniincammicia/lightboard">repository pubblico</a> e con l'attiva collaborazione della <a rel="nofollow" href="http://wiildos.it/">community WiiLD</a>, e parallelamente si sta raccogliendo la documentazione tecnica necessaria al potenziamento di <a rel="nofollow" href="http://www.lampschool.it/">Lampschool</a>, il registro elettronico opensource. Nel corso del 2016 si intendono reiterare altre campagne analoghe ad intervalli regolari</li>
  <li>sul sito della campagna <a href="https://www.sistemainoperativo.it/">Sistema Inoperativo</a> sono state pubblicate maggiori informazioni relative al comportamento dei maggiori produttori di personal computer nei confronti del rimborso delle licenza Windows, in modo da facilitare la scelta dei prodotti hardware da acquistare per coloro che - legittimamente - non hanno intenzione di sperperare soldi in inutili licenze. Ricordate per il prossimo anno di <a href="{% link _posts/2015-10-14-segnala-i-sistemi-inoperativi.md %}">continuare a segnalarci</a> gli abusi da parte dei produttori!</li>
  <li>procede l'opera di monitoraggio degli <a href="https://lugmap.linux.it/eventi/" rel="nofollow">sportelli periodici di assistenza</a>, presso cui rivolgersi tutto l'anno per ricevere supporto ed aiuto su Linux. L'attività, segnalata sul portale delle Competenze Digitali dell'Agenzia per l'Italia Digitale, è complementare a quella di mappatura nazionale degli eventi occasionali, corsi incontri e serate, già in esecuzione <a href="{% link _posts/2015-07-11-quattro-anni-di-attivit.md %}">da quattro anni</a></li>
  <li>è stato aggiornato il <a href="https://planet.linux.it/lug/" rel="nofollow">Planet LUG</a>, piattaforma di aggregazione delle news provenienti dalle decine di Linux Users Group che popolano il territorio italiano</li>
  <li>come sempre va menzionato il <a href="https://www.linuxday.it/">Linux Day</a>, la principale manifestazione italiana per la promozione e la divulgazione del software libero. Che nel 2016 potrebbe raddoppiare e diventare internazionale, grazie a recenti contatti avuti con colleghi linuxari distribuiti in altri Paesi ed interessati al format dell'iniziativa nostrana. Più in generale uno dei propositi per il prossimo anno è quello di estendere e rafforzare i rapporti con altre realtà europee analoghe ad Italian Linux Society, per scambiare esperienze, condividere gli sforzi e potenziare le campagne di comunicazione e divulgazione</li>
</ul>

Per restare costantemente aggiornati su ciò che succede nel mondo del software libero in Italia ricordiamo <a href="/newsletter">la nostra newsletter</a> ed <a href="https://twitter.com/ItaLinuxSociety" rel="nofollow">il nostro account Twitter</a>.

E, per chi volesse partecipare in modo tangibile e concreto, ricordiamo la possibilità di associarsi alla nostra associazione per il 2016: con una singola quota di iscrizione di contribuisce concretamente al programma di raccolta fondi a sostegno dello sviluppo software, si gode di <a href="/iscrizione">tutti i benefici riservati ai soci ILS</a> e si può prendere parte attiva alla pianificazione delle iniziative.
