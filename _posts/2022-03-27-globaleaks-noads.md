---
layout: post
title: Quattro Chiacchere su GlobaLeaks e NoAds
image: /assets/posts/images/interview.jpg
image_copy: Photo by <a href="https://unsplash.com/@markuswinkler?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Markus Winkler</a> on <a href="https://unsplash.com/s/photos/magnifier?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
---

Dopo l'<a href="{% link _posts/2022-01-11-quattro-chiacchere-federico-terzi.md %}">intervista a Federico Terzi</a> abbiamo rivolto qualche altra domanda a Giovanni Pellerano e Giovanni Francesco Solone, rispettivamente referenti per i progetti GlobaLeaks e NoAds, altri due progetti Made in Italy coinvolti nel round di <a href="{% link _posts/2021-12-29-donazioni-2021.md %}">donazioni che ILS ha erogato a fine 2021</a>.

<!--more-->

---

Come è nato GlobaLeaks?

> GlobaLeaks è nato in un contesto di hacktivism internazionale, indicativamente a fine 2010, in un momento in cui hacker di tutto il mondo si interrogavano sul successo/insuccesso di Wikileaks e cercavano di capire come ripetere un simile progetto in modo più resiliente sia dal punto di vista tecnologico che dal punto di vista operativo al fine di consentire alla società civile di affrontare i propri problemi portandoli alla luce di chi possa affrontarli e senza che ve ne si  ripercussione per alcuno.
> In questo contesto c'era anche il nostro iniziale [team di sviluppo](https://github.com/globaleaks/GlobaLeaks/blob/devel/AUTHORS) con la voglia di supportare il fenomeno e con l'idea di poterlo amplificare scrivendo un software che mettesse insieme sicurezza ed usabilità e che permettesse a quell'idea di replicarsi dove ce ne fosse bisogno. Ed è così che nasce GlobaLeaks soluzione di whistleblowing anonimo libera ed opensource che partendo quasi per gioco tra un gruppo di 5 amici e trovando il supporto di incubazione dell'[Open Technology Fund di Washington](https://www.opentech.fund/results/supported-projects/globaleaks/) porta oggi ad oltre 10 anni di progetto ad una grande comunità di implementatori, provider ed utenti, ad una sua traduzione in oltre 90 lingue e ad una adozione da parte di oltre 4000 organizzazioni nel mondo in [contesti d'uso](https://www.globaleaks.org/usecases/) che spaziano dalla compliance aziendale, alla anticorruzione nel settore pubblico e alla difesa dei diritti umani in contesti ad alto rischio.

Cosa hai imparato da questa esperienza sia come sviluppatore che come persona?

> Sviluppare GlobaLeaks, che rappresenta il progetto su cui ho al momento maggiormente lavorato, è stato determinante per quello che oggi sono.
> Se quando ho cominciato avevo un po' di formazione ingegneristica e buona comprensione di architetture hardware, software e networking mai mi sarei immaginato della complessità del realizzare un prodotto nella sua interezza.
> Ho appreso negli anni come scrivere un software non voglia dire scrivere né qualcosa che funzionicchia né tantomeno qualcosa che funziona quanto qualcosa che possa continuare a funzionare nel tempo offrendo garanzie ai suoi utenti e continuando a costruirne sulle garanzie raggiunge. E con particolare riferimento all'open-source ho imparato quanto questo sia possibile solo con grazie a una comunità attiva e collaborativa in cui partecipare, raccogliendo requisiti e sviluppando e migliorando soluzioni. E con questo non mi riferisco allo sviluppo della propria codebase interna al progetto quanto proprio alle inter-collaborazioni fra progetti e componenti in cui è fondamentale partecipare nelle comunità affini e determinare lo sviluppo e la crescita di dei software e componenti su cui il tuo si fonda.. Ed è così che negli hanni ho accresciuto passione nel supportare un progetto quì ed un progetto là, su tematiche di usabilità o tematiche di testing supportando in altri progetti la comprensione delle problematiche approfondite nel nostro.

Cosa ne pensi del panorama FOSS italiano e cosa si può fare per migliorarlo?

> Credo che l'italia a livello informatico e FOSS abbia sempre vantato di comunità di sviluppatori con grande competenza e molti progetti di spicco a livello mondiale sia per progetti propri che per progetti a cui la comunità FOSS italiana ha contribuito. Evito di fare una carrellata di nomi perchè ci sono infiniti progetti validi e non vorrei fare torto a nessuno. E anche a livello di utenti ritengo ci sia stato sempre molto interesse nella comprensione della tecnologia come fattore abilitante per la società civile (mi vengono in mente l'entusiasmo e  il diffondersi rapido dei  LUG a fine anni 90). Il limite delle comunità FOSS italiane secondo me è che queste purtroppo rimangono secondo molto frazionate credo a causa forse della mancanza di risorse e investimenti a livello paese e della alta competizione che ne deriva e che porta ad ulteriore divisione. Questo penso sia dovuto in gran parte al fatto che a livello paese FOSS in italia sia stato troppo a lungo stato superficialmente compreso solo per i suoi vantaggi di gratuità per cui è stato utilizzato spesso classificato solo come alternativa a soluzioni commerciali e costose come se il costo commerciale e il brevetto di una soluzione  fossero l'unico elemento di garanzia della sua qualità.
> Riconosco che la situazione e le sensibilità stiano recentemente cambiando (vedi come AgID stia finalmente galdeggiando se non obbligando  la pubblica amministrazione ad adoperare software opensource) e questo è certamente merito dell'impegno continuo e costante di associazioni  come ILS che con costanza da anni mettono a disposizione il loro tempo, la loro esperienza, la loro passione e anche talvolta le loro poche risorse economiche (spesso personali) pur di supportare e comunicare i progetti esistenti determinando loro stesse un cambiamento.

---

Come è nato NoAds?

> Per puro caso. Ho iniziato a usare Adblock all'epoca del suo primo rilascio, ho pensato si trattasse di un'estensione quasi irrinunciabile considerato il sempre crescente proliferare di pubblicità invasiva nei siti web che si visitano quotidianamente. Da quell'ormai lontano 2007 di anni ne sono passati tanti e io ho cambiato componente aggiuntivo preferito (oggi uBlock), ho cambiato modo di scrivere i filtri, metodo di rilascio e una marea di altre cose. Il progetto è ancora in piedi, ha ricevuto in dono una casa tutta sua qualche anno fa e ancora oggi continuo "per diletto" e perché so di essere diventato un buon punto di riferimento per tantissimo utenti lì fuori.

Cosa hai imparato da questa esperienza sia come sviluppatore che come persona?

> Tanto. Ho imparato che talvolta quello che nasce e che fai per te può essere in realtà d'aiuto per molti altri, è un po' lo spirito alla base dell'Open Source e questo non può che migliorare ulteriormente le conoscenze e la collaborazione con altri utenti (che siano utilizzatori semplici o "colleghi" di progetto). Nel corso degli anni ho ricevuto tante email di supporto e anche nei momenti più "difficili", quando non riuscivo a capire se potesse ancora valere la pena investire su questo side project ho scelto di continuare a lavorarci, raggiungendo risultati mai sperati. Ancora di tanto in tanto conosco gente che non mi ha mai sentito o visto in faccia prima e che poi scopre che mantengo le liste NoAds X Files e mi ringrazia per il lavoro che svolgo, fa piacere!

Cosa ne pensi del panorama FOSS italiano e cosa si può fare per migliorarlo?

> Siamo in tanti ma mai abbastanza. C'è voglia, c'è grinta, c'è tanta conoscenza, eppure spesso ci si ferma, ci si dà per vinti, talvolta non si parte neanche. L'unione in questo caso non può che fare la forza e lì fuori ci sono tanti ragazzi con buona volontà e grandi capacità che potrebbero essere messe a disposizione del mondo Free e Open Source per raggiungere risultati al pari dei nostri vicini europei ma non solo. Dato però che non tutti nascono sviluppatori (o per lo meno in grado di muoversi e sapere dove provare a mettere le mani), bisognerebbe dare la possibilità a chi ha le idee di trovare gente capace di metterle in pratica, "tradurle" in codice, il tutto senza dimenticarsi che FOSS non vuol dire "a scrocco", le aziende dovrebbero sostenere idee e sviluppo quando si fa uso di questo tipo di software, non solo ringraziare e continuare a guadagnare sulle spalle altrui.

Come contribuire a NoAds

> È semplice. Il progetto è completamente aperto e disponibile [su GitHub](https://github.com/gioxx/xfiles). Si può sempre aprire una Issue per segnalare pagine da analizzare e ripulire, si può utilizzare lo strumento integrato nei componenti aggiuntivi (come AdBlock Plus) oppure si può dare direttamente una mano modificando [i file dei filtri](https://github.com/gioxx/xfiles/tree/master/contrib) (c'è un file README pubblicato proprio in quella directory che spiega ogni cosa). Se non si è capaci di intervenire "tecnicamente" si può sempre dare una mano contribuendo al sostentamento delle spese di progetto (hosting, ecc.) tramite i vari canali messi a disposizione (vedi il blocco ["Sostieni il progetto"](https://xfiles.noads.it/#license)).
