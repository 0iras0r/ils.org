---
layout: post
title: 'LibreUmbria: la Migrazione Possibile'
created: 1364343167
---
<p>
In occasione del <a rel="nofollow" href="http://www.documentfreedom.org/">Document Freedom Day 2013</a>, giornata mondiale dedicata ai formati liberi e aperti, <a rel="nofollow" href="http://libreumbria.wordpress.com/">LibreUmbria</a> ed <a href="/">Italian Linux Society</a> hanno realizzato un breve filmato di presentazione del progetto LibreUmbria.
</p>
<p style="text-align: center; width: 100%">
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WtvjrDRP99Y?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</p>
<p>
LibreUmbria è, in breve, l'iniziativa di migrazione a <a rel="nofollow" href="http://www.libreoffice.org/">LibreOffice</a> degli uffici pubblici della Regione Umbria. Opera niente affatto scontata, considerando la quantità di persone coinvolte nella transizione e le normali difficoltà tecniche ad essa legata, ma dagli indubbi vantaggi di carattere sociale, politico ed economico: maggiore interoperabilità, minori vincoli tecnologici, più garanzie sull'accesso ai documenti ed ai dati in essi contenuti, e minori costi a fondo perduto per le licenze software.
</p>
<p>
Il progetto LibreUmbria si distingue per la metodologia applicata, che pone massima attenzione alla persona ed alla sua consapevolezza del cambiamento. Come più volte ricordato <a rel="nofollow" href="http://www.youtube.com/watch?v=WtvjrDRP99Y">nel video</a>, infatti, i maggiori ostacoli alla migrazione molto spesso non sono di natura tecnica bensì culturale, associati ad uno scarso coinvolgimento di coloro cui viene chiesto il passaggio da un ambiente (virtuale) di lavoro all'altro; solo una continua e puntuale opera di formazione ed informazione possono permettere il successo dell'iniziativa, portando inoltre con sé positive ricadute nell'approccio agli strumenti adottati ed al modo di utilizzarli.
</p>
<p>
Infine, l'aspetto forse più interessante dell'intero progetto LibreUmbria è la completa trasparenza e lo spirito di condivisione con cui esso viene condotto: <a rel="nofollow" href="http://libreumbria.wordpress.com/">sul sito relativo</a> viene regolarmente pubblicata ricca documentazione sulle fasi progettuali ed operative, al dichiarato scopo di rendere più semplice e rapido ad altre amministrazioni statali la valutazione e l'attuazione di simili piani massivi di migrazione ed aggiornamento delle rispettive dotazioni digitali. Una scelta destinata ad avere un ampio impatto nel mondo della pubblica amministrazione italiana, che può trovare oggi nella Regione Umbria uno spunto ed un esempio da imitare per perseguire gli obiettivi strategici di informatizzazione e digitalizzazione.
</p>
<p>
Grazie agli amici umbri per l'amichevole partecipazione nella realizzazione di questo video e, soprattuto, per dimostrarci concretamente e materialmente come il software libero non sia solo più una alternativa secondaria ma una vera e propria scelta consapevole e motivata.
</p>
