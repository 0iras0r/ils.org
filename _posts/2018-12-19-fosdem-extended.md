---
layout: post
title: FOSDEM Extended
created: 1545217457
---
Il <a rel="nofollow" href="https://fosdem.org/">Free Open Source Developers European Meeting (FOSDEM)</a> si svolge ogni anno a Bruxelles ad inizio febbraio e attira circa 8000 addetti ai lavori del mondo opensource: sviluppatori, sistemisti e operatori IT di ogni sorta, ma anche appassionati ed entusiasti. È di fatto la più grande conferenza dedicata a questo tema in Europa, e negli anni è diventato un appuntamento imperdibile per tanti. Tra cui molti italiani, la cui presenza è certo notevole.

Ma non tutti hanno modo e maniera di partecipare, e da qui emerge il proposito di auto-organizzarsi per rendere l'evento accessibile a più persone. Il <a href="https://fosdem.linux.it/" rel="nofollow">FOSDEM Extended</a> è una nuova iniziativa, un invito rivolto a coloro che vogliono contribuire a creare un nuovo momento di aggregazione, confronto e conoscenza nella propria città. Per aderire basta uno spazio disponibile nel weekend del FOSDEM (quest'anno: sabato 2 e domenica 3 febbraio), un computer, un proiettore ed un paio di casse audio; registrando la propria istanza sul sito dedicato, gli aspiranti partecipanti potranno votare i talk che vorranno seguire in diretta streaming (scegliendo tra i tantissimi che saranno pubblicati nelle prossime settimane <a rel="nofollow" href="https://fosdem.org/2019/schedule/">nel programma 2019</a>) e costruire insieme la playlist della giornata, da fruire insieme magari con una pizza ed una birra (rigorosamente belga!).

Dato il taglio dei contenuti del FOSDEM - meno divulgativi di quelli abitualmente presentati al <a href="https://www.linuxday.it/">Linux Day</a>, e decisamente più tecnici - l'evento si rivolge soprattutto a professionisti, aziende, utenti esperti e "smanettoni", e può essere una occasione non solo per un approfondimento ed un aggiornamento tecnologico ma anche per fare networking tra le realtà professionali della propria area.
