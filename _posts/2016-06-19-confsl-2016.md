---
layout: post
title: ConfSL 2016
created: 1466294023
---
<p style="text-center">
<img src="/assets/posts/images/confsl-classic.png" style="display: block; margin: auto" />
</p>

<p>
Il 24/25/26 giugno torna la <a rel="nofollow" href="http://www.confsl.org/">ConfSL</a>, annuale conferenza sullo stato del software libero in Italia, per questa edizione ospitata a Palermo.
</p>

<p>
La manifestazione intende essere un momento di incontro e riflessione tra i membri della community freesoftware italiana, per valutare e misurare quanto compiuto nell'anno precedente e dove puntare per l'anno successivo. Oltre a ciò numerosi sono i talk e gli ospiti in programma, tra cui Alessandro Rubini, vice-presidente di <a rel="nofollow" href="https://fsfe.org/">Free Software Foundation Europe</a, e John Sullivan, direttore di <a rel="nofollow" href="http://fsf.org/">Free Software Foundation</a>.
</p>

<p>
Come ogni anno, noi di Italian Linux Society ci saremo e speriamo di trovarvi numerosi!
</p>
